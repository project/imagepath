Image Path Module
=================

This module allows you to refer to images that are associated with imagefields
using a simple URL structure that locates the images based on the nodes they belong
to, rather than their locations on the filesystem.  This is especially convenient
for situations where you might be changing the actual filepath in an imagefield
sometimes, but it's also nice if you just prefer the idea of having a more consistent
URL structure for your site, where images can be treated more like "first-class content".

When you install this module, it scans all your content types to find which ones
have imagefields.  It also "notices" when you alter any content types to add or remove
imagefields.  Without any configuration from you, it allows you to use URLs like this
to refer to images:

  /[$node_type]/[$nid]/[$field_name]

...where [$node_type] is the "machine-readable" name of your content type, [$nid] is the
node id for a particular node, and [$field_name] is the name of the imagefield *without*
the "field_" prefix.

So, for instance, if you have a content type called "biography", and an imagefield
defined for it called "field_portrait", you can refer to the portrait on node 34 like:

  /biography/34/portrait

As a "bonus", if your content type has only one imagefield, then you can omit the
field name at the end of the URL, like so:

  /biography/34
  
Note that this module works by intercepting the request URI very early in the request
processing cycle (via hook_init()), so it may very well interfere with paths that you
define using modules like Path or Pathauto.  Be careful.

Also please note that this module does not currently support imagefields with multiple
values.
