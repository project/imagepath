<?php

/**
 * Scan currently defined node types, and their
 * fields, and update the regexes that will
 * match requests for image fields.  These regexes,
 * plus the list of imagefields available for each
 * type, are returned in a structured array that defines
 * the "domain" over which imagepath will operate.  The
 * domain is also stored in a variable for caching.
 */
function _imagepath_update_domain() {
  $domain = array();
  foreach (content_types() as $type) {
    $type_added = FALSE;
    $imagefield_count = 0;
    foreach ($type['fields'] as $field) {
      if ($field['type'] == 'image') {
        $imagefield_count++;
        if (!$type_added) {
          $type = $type['type'];
        }
        $field_name = substr($field['field_name'], 6);
        $domain[$type]['fields'][] = $field_name;
        $type_added = TRUE;
      }
    }
    if ($type_added) {
       $regex .= '))';
      // Allow field name to be omitted if type has only one imagefield.
      if ($imagefield_count == 1) {
        $regex .= '?';
        $domain[$type]['default_field'] = $field_name;
      }
    }
  }
  variable_set('imagepath_domain', $domain);
  return $domain;
}
